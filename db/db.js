const mongoose = require('mongoose');
const keys = require('../env/keys');

let URL = keys.DB_URL;

if(['production'].includes(process.env.NODE_ENV))
    URL = process.env.DB;

mongoose.connect(URL,{
    useFindAndModify: false,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
});