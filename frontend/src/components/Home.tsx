import React, { useState,useEffect } from 'react';
import Navbar from './shared/Navbar'
import Sidebar from './shared/Sidebar'
import { BrowserRouter,Route,Switch, useHistory } from 'react-router-dom';
import HomeContent from './HomeContent/HomeContent';
import Login from './Auth/Login';
import AboutUs from './AboutUs/AboutUs';
import Folders from './Dashboard/Folders';
import GoogleAuth from './Auth/GoogleAuth';
import { Provider, useDispatch } from 'react-redux'
import store from '../redux/Store';
import { logoutUser } from '../redux/uReducer/Actions';
import ProtectedRoute from './shared/ProtectedRoute';

function Home(props: any) {
    const dispatch = useDispatch();
    const [state,setState] = useState({
        open: false
    })


    useEffect(() => {
        // GET THE EXPIRY OF USER TOKEN
        let exp = parseInt(localStorage.getItem('exp') || '0');
    
        // IF CURRENT DATE IS GREATER THAN EXPIRY DATE LOGOUT USER
        if(Date.now() > exp) {
            dispatch(logoutUser);
        }
      });
  

    const toggleDrawer = (val: any) => {
        setState({
            ...state,
            open: val
        });
    }

    return (
        <div>
            <BrowserRouter>
                <Navbar toggleDrawer={toggleDrawer} />
                <Sidebar open={state.open} toggleDrawer={toggleDrawer}/>
                <div className="mt-5"></div>
                    <Switch>
                        <Route path="/" exact component={HomeContent} />
                        <Route path="/login" exact component={Login} />
                        <Route path="/aboutus" exact component={AboutUs} />
                        <ProtectedRoute path="/_" exact component={Folders} />
                        <Route path="/google/qfTT5TSkhFKupDtbohnwPheYFODWcmkr9HgZ8MbAElk09VzJPxKBPLRpZVtHQCfENK2lFUBSIc4XY7O35pigP4TK597p0XzYqugB" exact component={GoogleAuth} />
                    </Switch>
             </BrowserRouter>
        </div>
    )
}

export default Home
