import React from 'react'
import SVGStorage from '../../images/storage.svg';
import SVGSecure from '../../images/securedata.svg';
import { Button, Icon } from '@material-ui/core';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';


function HomeContent() {
    return (
        <>
            <div className="container-fluid mt-5 mb-5 pb-5">
                 <div className="row mt-5 pt-5">
                     <div className="col-md-6 col-sm-6 col-12 my-auto">
                        <h2 className="text-uppercase home-title" id="handleFilesTitle">Want to handle files and folders?</h2>
                        <br />
                        <p className="text-white-50 home-content">
                            This plateform provides storage for files using folder structure. Get the feel of file system using this plateform.
                        </p>
                        <Button className="btn-outline-orange px-4 mt-2" id="getStarted">
                            Get Started
                        </Button>
                     </div>
                     <div className="col-md-6 col-sm-6 col-12">
                        <img src={SVGSecure} className="img-fluid w-100" />
                     </div>
                 </div>
             </div>

             <div className="container-fluid mt-0 mt-md-5 pt-0  pt-md-5">
                 <div className="row mt-0 mt-md-5 pt-0  pt-md-5">
                     <div className="col-md-6 col-sm-6 col-12">
                        <img src={SVGStorage} className="w-100" height="550" />
                     </div>
                     <div className="col-md-6 col-sm-6 col-12 my-auto">
                        <h2 className="text-uppercase home-title">Lost yours files ?</h2>
                        <br />
                        <p className="text-white-50 home-content">
                            Store your files and access them whenever you want in a easy and efficient way.
                        </p>
                     </div>
                 </div>
             </div>

            <div className="container-fluid mt-0 mt-md-5 pt-0  pt-md-5">

                <div className="row mt-0 mt-md-5 pt-0  pt-md-5">
                    <div className="col-12 text-center">
                        <h3 className="text-uppercase home-title">
                            <Icon fontSize="large">
                                <EmojiObjectsIcon  fontSize="large"/>
                            </Icon>  WHAT WE OFFER?
                        </h3>
                    </div>

                    <div className="col-md-4 col-sm-6 mt-5">
                    <div className="card text-center h-100">
                        <div className="__content mt-2">
                        <h3 className="__title pb-3">Folder Structure</h3>
                        <p>Access files in a folder structure that is same as you access hard drives.</p>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-4 col-sm-6 mt-5">
                    <div className="card text-center h-100">
                        <div className="__content mt-2">
                        <h3 className="__title pb-3">Files</h3>
                        <p>Store files in whichever folder you want and access the files using that folder.</p>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-4 col-sm-6 mt-5">
                    <div className="card text-center h-100">
                        <div className="__content mt-2">
                        <h3 className="__title pb-3">Download Files</h3>
                        <p>
                            Download the files stored
                        </p>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-4 col-sm-6 mt-5">
                    <div className="card text-center h-100">
                        <div className="__content mt-2">
                        <h3 className="__title pb-3">Rename Files/Folders</h3>
                        <p>
                            Rename the files nad folders according to your choice.
                        </p>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-4 col-sm-6 mt-5">
                    <div className="card text-center h-100">
                        <div className="__content mt-2">
                        <h3 className="__title pb-3">Properties</h3>
                        <p>
                            See the stats of the folder and files (size, no of files in folders).
                        </p>
                        </div>
                    </div>
                    </div>
                    <div className="col-md-4 col-sm-6 mt-5">
                        <div className="card text-center h-100">
                            <div className="__content mt-2">
                            <h3 className="__title pb-3">Lorem ipsum</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta dolor praesentium at quod autem omnis, amet eaque unde perspiciatis adipisci possimus quam facere illo et quisquam quia earum nesciunt porro.</p>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
             <br />
             <br />
             <br />
             <br />
             <br />
             <br />
        </>
    )
}

export default HomeContent
