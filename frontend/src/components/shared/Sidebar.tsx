import React, { useState } from 'react'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import { AppBar,Toolbar, Typography, Button, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SVGAvatar from '../../images/avatar.svg';
import { Link, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logoutUser } from '../../redux/uReducer/Actions';
import { ReduxUserReducerStateInterface } from '../../interfaces/ReduxUserReducerStateInterface';
import Logo from '../../images/logo.png';
import { UserInterface } from '../../interfaces/UserInterface';

function Sidebar(props: any) {
    const history = useHistory();
    const dispatch = useDispatch();

    const user: ReduxUserReducerStateInterface = useSelector((state: any) => state.User);


    //  LOGOUT USER
    const logout = () => {
        dispatch(logoutUser());
        history.push('/');
    }
  
  
    return (
        <Drawer className="drawer" anchor="left" open={props.open} onClose={() => props.toggleDrawer(false)} >
                <Toolbar className="Navbar">
                    <Typography variant="h6" className="nav-icon">
                        <img id="sidebar-icon" src={Logo} />
                    </Typography>
                    <span style={{flexGrow: 1}}></span>
                    <IconButton className="text-white" onClick={() => props.toggleDrawer(false)}>
                        <CloseIcon />
                    </IconButton>
                </Toolbar>

            {user.isAuthenticated && 
                <div className="container-fluid text-center mt-2">
                    <img src={SVGAvatar} width="150" />

                    <h4 className="mt-2 text-orange">
                        { (user.user as UserInterface).username }
                    </h4>
                    <Button className="bg-white text-black px-4 mt-2" onClick={logout}>
                        <i className="fa fa-sign-out"></i>&nbsp;Logout
                    </Button>
                </div>
            }


             <List className="text-white">
                    <ListItem button>
                        <ListItemIcon className="text-orange"> 
                            <HomeIcon />
                        </ListItemIcon>
                        <b>
                        <Link id="sidebar-list-item" to="/"  className="text-white-50 sidebar-link text-decoration-none" >
                            <ListItemText  primary="Home" />
                        </Link>
                        </b>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon className="text-orange"> 
                            <InfoIcon />  
                        </ListItemIcon>
                        <b>
                        <Link id="sidebar-list-item" to="/aboutus"  className="text-white-50 sidebar-link text-decoration-none" >
                            <ListItemText primary="About Us" />
                        </Link>
                        </b>
                    </ListItem>
                    {user.isAuthenticated && 
                        <ListItem button>
                            <ListItemIcon className="text-orange"> 
                                <InfoIcon />  
                            </ListItemIcon>
                            <b>
                            <Link id="sidebar-list-item" to={"/_?id=" + (user?.user as UserInterface)?.folder?._id}  className="text-white-50 sidebar-link text-decoration-none" >
                                <ListItemText  primary="Folders" />
                            </Link>
                            </b>
                        </ListItem>
                    }
            </List>
        </Drawer>
    )
}

export default Sidebar
