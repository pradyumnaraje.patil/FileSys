import React from 'react';
import { ReduxUserReducerStateInterface } from '../../interfaces/ReduxUserReducerStateInterface';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router';

// PROTECTED ROUTE
function ProtectedRoute(props: any) {
    const user: ReduxUserReducerStateInterface = useSelector((state: any) => state.User); 
    const Component: any = props.component;
    return (
            user.isAuthenticated ? 
            <Route {...props} render={(props) => <Component {...props} /> } /> 
            : 
            <Redirect to="/login" {...props} />
    )
}

export default ProtectedRoute;