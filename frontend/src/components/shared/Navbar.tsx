import React from 'react'
import { AppBar,Toolbar, Typography, Button, IconButton } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import { useSelector, useDispatch } from 'react-redux';
import { ReduxUserReducerStateInterface } from '../../interfaces/ReduxUserReducerStateInterface';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { logoutUser } from '../../redux/uReducer/Actions';
import Logo from '../../images/logo.png';
import zIndex from '@material-ui/core/styles/zIndex';

function Navbar(props: any) {
    const history = useHistory();
    const dispatch = useDispatch();
    const user: ReduxUserReducerStateInterface = useSelector((state: any) => state.User);

    const login = () => {
        history.push({
            pathname: '/login'
        });
    }


    const logout = () => {
        dispatch(logoutUser());
        history.push('/');
    }
  
  


    return (
        <div>
            <AppBar color="primary" position="fixed" className="Navbar" style={{zIndex: 1000}} >
                <Toolbar>
                    <IconButton id="sidebar-toggle" className="text-white" onClick={() => props.toggleDrawer(true)}>
                        <MenuIcon  />
                    </IconButton>
                    <Typography variant="h6" className="nav-icon d-none d-sm-block">
                        <img id="navbar-icon" src={Logo} />
                    </Typography>
                    <span style={{flexGrow: 1}}></span>
                    { user.isAuthenticated == false ? 
                        <Button 
                            id="navbar-login"
                            size="medium"
                            className="px-3 btn-outline-orange"
                            onClick={login}
                        >
                            <i className="fab fa-google"></i>&nbsp;&nbsp;Login
                            </Button>
                        : 
                        <Button 
                            id="navbar-logout"
                            size="medium"
                            className="px-3 btn-outline-orange"
                            onClick={logout}
                        >
                            <i className="fa fa-sign-out"></i>&nbsp;&nbsp;Logout
                        </Button>
                    }
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navbar
