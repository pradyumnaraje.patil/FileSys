import React, { useState, useRef, useCallback, useEffect } from 'react';
import FolderIcon from '../../images/files/folder.png'
import { Link, useHistory } from 'react-router-dom';
import { FolderPropsInterface } from '../../interfaces/FolderInterface';
import '../../App.css';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { rename } from 'fs/promises';
import axios from 'axios';
import { SERVER_URL } from '../data/Variables';
import { getHeader } from '../../utils/getHeader';
import { FileInterface } from '../../interfaces/FileInterface';
import PropertiesDialog from '../../reusables/PropertiesDialog';
import DialogDelete from '../../reusables/DialogDelete';

const initialState = {
    clientX: 0,
    clientY: 0,
    selected: false,
    rename: false
}

function Folder(props: FolderPropsInterface) {
    const [state,setState] = useState(initialState);
    const [name,setName] = useState(props.name);
    const inputRef = useRef(null);
    const history = useHistory();
    const [propertyDialogOpen,setPropertyDialogOpen] = useState(false);
    const [deleteDialog, setDeleteDialog] = useState(false);


    const showContextMenu = (event: React.MouseEvent<HTMLDivElement,MouseEvent>) => {
        event.preventDefault();

        setState({
            ...initialState,
            clientX: event.clientX,
            clientY: event.clientY,
            selected: true,
        });
    }

    const handleClose = (idx: number) => (event: React.MouseEvent<HTMLLIElement,MouseEvent>) =>  {

        // HANDLE THE OPTIONS CLICKED
        if(idx != -1) {
            if(idx == 1){
                openFolder();
            } else if(idx == 2) {
                setState({
                    ...initialState,
                    selected: false,
                    rename: true
                });
                // WAIT FOR INPUT BOX TO BE VISIBLE
                setTimeout(()=>{
                    (inputRef.current as any).focus();
                },500)
            } else if(idx == 3) {
                setDeleteDialog(true);
            } else if(idx == 4) {
                showFolderProperties();
            }
        } else {
            setState(initialState);
        }
    }


    const onDeleteDialogClose = () => {
        setDeleteDialog(false);
    }
    const deleteFolder = async () => {
        await axios.delete(`${SERVER_URL}/folder/${props._id}`,getHeader());
        window.location.reload();
    }

    // SHOW THE PROPERTIES OF CURRENT FOLDER
    const showFolderProperties = () => {
        setState(initialState);
        setPropertyDialogOpen(true);
    }
    // CLOSE PROPERTY DIALOG
    const closePropertDialog = () => {
        setPropertyDialogOpen(false);
    } 

    // INPUT BOX
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    }

    // RENAME STATE CHANGED
    const closeRename = () => {
        setState({
            ...state,
            rename: false
        });
        if(name === '')
            setName(props.name);

        renameFolder(name);
    }

    const renameFolder = async (name: string) => {
        const result = await axios.put(`${SERVER_URL}/folder/${props._id}/rename`,{name},getHeader());
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if(event.key === 'Enter')
            closeRename();
    }

    const openFolder = () => {
        history.push(`/_?id=${props._id}`);
    }

    return (
        <div className="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 text-center" >
            {/* DIALOG FOR PROPERTIES */}
            <PropertiesDialog open={propertyDialogOpen} onClose={closePropertDialog} file={props} />
            {/* END OF DIALOG FOR PROPERTIES */}
            <DialogDelete open={ deleteDialog } onClose={ onDeleteDialogClose } onSuccess={ deleteFolder } fileName={ name }></DialogDelete>

        <div onContextMenu={showContextMenu}
             onDoubleClick={openFolder} 
             className={(state.selected == false) ? 'text-decoration-none folder-icon': 'text-decoration-none folder-icon folder-selected'} >
            <img src={FolderIcon} style={{width: '100px'}} />
            {
            (state.rename) ? 
                <input 
                    ref={inputRef}
                    type="text" 
                    className="w-75 mx-auto form-control" 
                    value={name} 
                    onBlur={closeRename}
                    onChange={handleChange}
                    onKeyPress={handleKeyPress}
                    /> 
            :
                <p className="text-orange">{name}</p>
            }
        </div>
        <Menu onClose={handleClose(-1)} 
            open={state.clientY != 0}  
            anchorReference="anchorPosition" 
            anchorPosition={{top:state.clientY,left: state.clientX}}>
                {
                    ["Open","Rename","Delete","Properties"].map((t,k) => (
                        <MenuItem onClick={handleClose(k + 1)} key={k}>{ t }</MenuItem>
                    ))
                }
        </Menu>
        </div>  
    )
}

export default Folder;