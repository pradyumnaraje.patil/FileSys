import React, { useState, useEffect } from 'react'
import { Button } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add';
import Folder from './Folder';
import { FolderPropsInterface } from '../../interfaces/FolderInterface';
import { getQueryParam } from '../../utils/QueryParam';
import axios from 'axios';
import { SERVER_URL } from '../data/Variables';
import { getHeader } from '../../utils/getHeader';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { DialogComponentInterface } from '../../interfaces/DialogComponentInterface';
import DialogComponent from '../../reusables/DialogComponent';
import { useDispatch, useSelector } from 'react-redux';
import { fileUpload, fileUploadError } from '../../redux/fReducer/Actions';
import File from './File';
import DialogMessage from '../../reusables/DialogMessage';
import Viewer from 'react-viewer';
import { Camera } from '@material-ui/icons';
import { isImage } from '../../utils/isImage';
import mime from 'mime-types';
import DialogUsers from '../../reusables/DialogUsers'

function Folders(props: any) {
    const [folder, setFolder] = useState<FolderPropsInterface>();
    const [loading,setLoading] = useState(false);
    const [dialog,setDialog] = useState<any>({
        open: false,
        title: 'File Uploaded',
    });
    const [imagePreview,setImagePreview] = useState(false);
    const [activeIdx,setActiveIdx] = useState(0);
    const [userDialog,openUserDialog] = useState(false);
    const [userDialogInfo,setUserDialogInfo] = useState({
        id: '',
        name: ''
    });
    let currentIndex: number = -1;


    const errorMessage = useSelector((state: any) => state.File.error);
    const onErrorDialogClose = () => {
        dispatch(fileUploadError(''));
    }

    const dispatch = useDispatch();

    useEffect(() => {
        // GET THE PARAMS
        setLoading(true);
        let id = getQueryParam(props.location.search,'id');
        getFolderData(id);
    },[props.location]);

    // GET CURRENT FOLDER DATA FROM DB
    const getFolderData = async (id: any) => {
        console.log(id);
        let result =  await axios.get(`${SERVER_URL}/folder/${id}`,getHeader());
        setFolder(result.data.data);
        console.log(result.data.data);
        setLoading(false);
    }

    const addFolder = async () => {
        let id = getQueryParam(props.location.search,'id');
        console.log(id);
        const newFolder = {name: 'New Folder'}
        let result = await axios.post(`${SERVER_URL}/folder/${id}/new`,{ ...newFolder },getHeader());

        getFolderData(id);
    }

    // 
    const openUploadDialog = () => {
        setDialog({
            ...dialog,
            open: true
        });
    }

    const onDialogClose = (x: number = 1) => {
        if(x == 0 || window.confirm("You will lose all your data if closed.")){
            setDialog({
                ...dialog,
                open: false
            });
        }
    }

    // GET FOLDERS
    const getFolders = folder?.folders.map((f, idx) => (
        <Folder key={idx} {...f} />
    ));

    const setUserDialog = (id: any,name: string) => {
        openUserDialog(true);
        setUserDialogInfo({id: id, name: name });
    }
    const closeUserDialog = () => {
        openUserDialog(false);
    }


    // SET ACTIVE INDEX TO BE PASSED TO FILES
    const setActive = (idx: number) => {
        setActiveIdx(idx);
        setImagePreview(true);
    }

    const getFiles = folder?.files.map((f,idx) => {
        if(isImage(mime.extension(f.type) || "")){
            currentIndex++;
        }
        return (
                <File  key={idx} {...f} idx={currentIndex} setUserDialog={setUserDialog} setActive={setActive} />
            )
    });

    const uploadFile = (fileData: any) => {
        let id = getQueryParam(props.location.search,'id');
        onDialogClose(0);
        dispatch(fileUpload(fileData,id))
    }

    const closeImagePreview = () => {
        setImagePreview(false);
    }

    const openImagePreview = () => {
        setImagePreview(true);
    }


    const getImages = (): any[] => {
        let pImg = folder?.files.filter((f,idx) => isImage(mime.extension(f.type) || ""));
        let d = pImg?.map((p, idx) => { return {src: p.link, alt: p.name}}) || [];
        console.log(d);
        return d;
    }

    return (
        <div className="container-fluid mt-5 position-relative" >
            <DialogComponent {...dialog} onDialogClose={onDialogClose} uploadFile={uploadFile}/>
            <DialogMessage open={ errorMessage != "" } onClose={ onErrorDialogClose } message={ errorMessage } ></DialogMessage>
            <DialogUsers open={userDialog} {...userDialogInfo} onClose={closeUserDialog}></DialogUsers>
            {/* IMAGES */}
            <Viewer 
                activeIndex={ activeIdx }
                downloadable={ false }
                visible={imagePreview} 
                onClose={closeImagePreview} 
                images={getImages() as any}  />
            {/* END OF IMAGES */}


            <div className="row mt-5 pt-5">
                <div className="col-12">
                    <Button variant="contained" className="btn shadow" 
                            startIcon={<AddIcon />}
                            onClick={addFolder}>
                        Add Folder
                    </Button>
                    <Button variant="contained" className="btn shadow mx-1 mt-1 mt-sm-0" 
                            startIcon={<CloudUploadIcon />}
                            onClick={openUploadDialog}>
                        Upload File
                    </Button>
                    <Button variant="outlined" 
                        className="text-white border-white" 
                        startIcon={<Camera />}
                        hidden={currentIndex == -1}
                        onClick={ openImagePreview }
                        >
                        Photo Viewer
                    </Button>
                </div>
            </div>
            <hr color="primary"/>
            <div className="container-fluid mt-1">
                { !loading && 
                    <div className="row">
                        { getFolders }
                        { getFiles }
                    </div>
                }
            </div>

        </div>
    )
}

export default Folders;
