import React, { useState, useRef, useCallback, useEffect } from 'react';
import FolderIcon from '../../images/files/folder.png'
import { Link, useHistory } from 'react-router-dom';
import { FolderPropsInterface } from '../../interfaces/FolderInterface';
import '../../App.css';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { rename } from 'fs/promises';
import axios from 'axios';
import { SERVER_URL } from '../data/Variables';
import { getHeader } from '../../utils/getHeader';
import { FileInterface } from '../../interfaces/FileInterface';
import mime from 'mime-types';
import PropertiesDialog from '../../reusables/PropertiesDialog';
import DialogDelete from '../../reusables/DialogDelete';
import { isImage } from '../../utils/isImage';
import Storage from '../data/Firebase';

const initialState = {
    clientX: 0,
    clientY: 0,
    selected: false,
    rename: false
}

function File(props: FileInterface) {
    const [state,setState] = useState(initialState);
    const [name,setName] = useState(props.name);
    const [FileIcon,setFileIcon] = useState('');
    const inputRef = useRef(null);
    const [propertyDialogOpen,setPropertyDialogOpen] = useState(false);
    const [deleteDialog, setDeleteDialog] = useState(false);

    useEffect((): any => {
        let type = props.type;
        let extension = mime.extension(type);

        async function getFileIcon() {
            try {
                console.log(extension);
                if(extension == 'jpeg') extension = 'jpg';
                let fileIcon = await import(`../../images/files/${extension}.png`);

                setFileIcon(fileIcon.default); 
            } catch(E) {
                let fileIcon = await import(`../../images/files/txt.png`);
                setFileIcon(fileIcon.default);
                console.log(E);
            }
        }

        if(type === 'dir') {
            setFileIcon(FolderIcon);
        } else {
            getFileIcon();
        }
    },[])

    const showContextMenu = (event: React.MouseEvent<HTMLDivElement,MouseEvent>) => {
        event.preventDefault();

        setState({
            ...initialState,
            clientX: event.clientX,
            clientY: event.clientY,
            selected: true,
        });
    }

    const handleClose = (idx: number) => (event: React.MouseEvent<HTMLLIElement,MouseEvent>) =>  {

        // HANDLE THE OPTIONS CLICKED
        if(idx != -1) {
            if(idx == 1){
                downloadFile();
            } else if(idx == 2) {
                setState({
                    ...initialState,
                    selected: false,
                    rename: true
                });
                // WAIT FOR INPUT BOX TO BE VISIBLE
                setTimeout(()=>{
                    (inputRef.current as any).focus();
                },500)
            } else if(idx == 3) {
                setDeleteDialog(true);
            } else if(idx == 4) {
                showFolderProperties();
            } else if(idx == 5) {
                setState(initialState);
                props.setUserDialog(props._id,props.name);
            } else if(idx == 6) {
                setState(initialState);
                shareWhatsapp();
            } if(idx == 7) {
                setState(initialState);

                props.setActive(props.idx);
            }
        } else {
            setState(initialState);
        }
    }

    const shareWhatsapp = () => {
        var walink = document.createElement('a');
        walink.setAttribute('href',`https://api.whatsapp.com/send?text=Download File: ${encodeURIComponent(props.link)}`);
        walink.setAttribute('data-action','share/whatsapp/share');
        walink.setAttribute('target','_blank');
        walink.click();
    }

    const onDeleteDialogClose = () => {
        setDeleteDialog(false);
    }

    const deleteFile = async () => {
        await axios.delete(`${SERVER_URL}/file/${props._id}`,getHeader());
        await Storage.refFromURL(props.link).delete();
        window.location.reload();
    }
    
    // SHOW THE PROPERTIES OF CURRENT FOLDER
    const showFolderProperties = () => {
        setState(initialState);
        setPropertyDialogOpen(true);
    }
    // CLOSE PROPERTY DIALOG
    const closePropertDialog = () => {
        setPropertyDialogOpen(false);
    } 

    // INPUT BOX
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    }

    // RENAME STATE CHANGED
    const closeRename = () => {
        setState({
            ...state,
            rename: false
        });
        if(name === '')
            setName(props.name);

        renameFile(name);
    }

    const renameFile = async (name: string) => {
        console.log(props._id);
        const result = await axios.put(`${SERVER_URL}/file/${props._id}/rename`,{name},getHeader());
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if(event.key === 'Enter')
            closeRename();
    }

    const downloadFile = () => {
        window.location.href = props.link;
    }

    return (
        <div className="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 text-center" >

            {/* DIALOG FOR PROPERTIES */}
            <PropertiesDialog open={propertyDialogOpen} onClose={closePropertDialog} file={props} />
            {/* END OF DIALOG FOR PROPERTIES */}
            <DialogDelete open={ deleteDialog } onClose={ onDeleteDialogClose } onSuccess={ deleteFile } fileName={ name }></DialogDelete>

        <div onContextMenu={showContextMenu}
             className={(state.selected == false) ? 'text-decoration-none folder-icon': 'text-decoration-none folder-icon folder-selected'} >
            <img src={FileIcon as any} style={{width: '100px'}} />
            {
            (state.rename) ? 
                <input 
                    ref={inputRef}
                    type="text" 
                    className="w-75 mx-auto form-control" 
                    value={name} 
                    onBlur={closeRename}
                    onChange={handleChange}
                    onKeyPress={handleKeyPress}
                    /> 
            :
                <p className="text-orange">{name}</p>
            }
        </div>
        <Menu onClose={handleClose(-1)} 
            open={state.clientY != 0}  
            anchorReference="anchorPosition" 
            anchorPosition={{top:state.clientY,left: state.clientX}}>
                {
                    isImage(mime.extension(props.type) || "") && <MenuItem onClick={handleClose(7)} key={6}>Open</MenuItem>
                }
                {
                    ["Download","Rename","Delete","Properties","Share via Email","Share with Whatsapp"].map((t,k) => (
                        <MenuItem onClick={handleClose(k + 1)} key={k}>{ t }</MenuItem>
                    ))
                }
        </Menu>
        </div>  
    )
}

export default File;