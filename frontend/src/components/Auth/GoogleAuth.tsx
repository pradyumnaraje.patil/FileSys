import React, { useEffect } from 'react';
import SpinnerComponent from '../../reusables/SpinnerComponent';
import { getQueryParam } from '../../utils/QueryParam';
import { useHistory } from 'react-router';
import { decodeJWT } from '../../utils/JwtDecode';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../redux/uReducer/Actions';

const GoogleAuth = (props: any) => {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect( () =>{
        let token: string | null = getQueryParam(props.location.search,'token');

        // REDIRECT IF TOKEN IS EMPTY
        if(token == null) 
            history.push('/login');
        else {
            // DECODE THE RECEIVED TOKEN
            let decodedToken = decodeJWT(token);
            // STORE THE TOKEN IN LOCALSTORAGE
            localStorage.setItem('user',token);
            localStorage.setItem('exp',(Date.now() + 3600 * 24 * 1000) as any);
            dispatch(loginSuccess(decodedToken as any));
            history.push('/');
        }
    });

    return (
        <div className="h-100 mt-5 pt-5">
            <div className="mx-auto text-center mt-5 pt-5" >
                <SpinnerComponent className="text-orange" size={100}/>
            </div>
        </div>
    )
}

export default GoogleAuth;

