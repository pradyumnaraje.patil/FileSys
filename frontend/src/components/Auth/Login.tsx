import { Card, CardContent, List,ListItem,ListItemText } from '@material-ui/core'
import React, { useEffect } from 'react'
import SVGLogin from '../../images/login.svg';
import { Button } from '@material-ui/core';
import LockIcon from '@material-ui/icons/Lock';
import { SERVER_URL } from '../data/Variables';
import { useSelector } from 'react-redux';
import { ReduxUserReducerStateInterface } from '../../interfaces/ReduxUserReducerStateInterface';
import { useHistory } from 'react-router';

function Login() {
    const history = useHistory();
    const uReducer: ReduxUserReducerStateInterface = useSelector((state: any) => state.User)

    useEffect(() => {
        if(uReducer.isAuthenticated) 
            history.push('/');
    });

    const googleLogin = () => {
        window.location.href = `${SERVER_URL}/auth/google`;
    }

    return (
        <div className="container mt-5 pt-5">
            <Card className="card px-1">
                <CardContent className="p-1">
                    <div className="d-none d-lg-block">
                    <div className="row ">
                        <div className="col-6">
                            <img src={SVGLogin} className="img-fluid w-100"/>
                        </div>
                        <div className="col-6 text-center my-auto">
                            <Button 
                                onClick={googleLogin}
                                startIcon={<LockIcon />}
                                className="btn-outline-orange px-5 py-2" 
                                size="large">
                                Login With Google
                            </Button>
                        </div>
                    </div>
                    </div>
                    <div className="d-block d-lg-none">
                    <div className="row">
                        <div className="col-12 text-center my-auto mx-auto pb-5 pb-lg-0">
                            <Button 
                                onClick={googleLogin}
                                startIcon={<LockIcon />}
                                className="bg-white btn-orange text-small" 
                                size="large">
                                Login With Google
                            </Button>
                        </div>
                        <div className="col-12 w-100">
                            <img src={SVGLogin} className="img-fluid w-100 p-0"/>
                        </div>
                    </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export default Login
