import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyAk43bN7kFaXlOXj8sFi9dqC28JPnH5xk4",
    authDomain: "filesys-a18f1.firebaseapp.com",
    projectId: "filesys-a18f1",
    storageBucket: "filesys-a18f1.appspot.com",
    messagingSenderId: "990612541073",
    appId: "1:990612541073:web:5ae3cc5280a2cc996eb930"
};

firebase.default.initializeApp(firebaseConfig);

export default firebase.default.storage();