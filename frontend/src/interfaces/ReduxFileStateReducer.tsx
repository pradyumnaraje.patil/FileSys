export interface ReduxFileStateReducer {
    loading: boolean,
    message: string,
    error: string
}