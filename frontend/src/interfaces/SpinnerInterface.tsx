export interface SpinnerInterface {
    size: number,
    className: string
}