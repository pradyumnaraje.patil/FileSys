import { FileInterface } from "./FileInterface";
import { FolderPropsInterface } from './FolderInterface';

export interface PropertiesDialogInterface {
    open: boolean,
    onClose: any,
    file: FileInterface | FolderPropsInterface
}