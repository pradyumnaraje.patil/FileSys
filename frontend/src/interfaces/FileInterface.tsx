export interface FileInterface {
    _id: string,
    name: string,
    type: string,
    link: string,
    size: number,
    uploadedBy?: string,
    setActive: any,
    idx: number,
    setUserDialog: any
}