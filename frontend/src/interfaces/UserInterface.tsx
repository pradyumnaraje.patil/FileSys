import { FolderPropsInterface } from "./FolderInterface";

export interface UserInterface {
    googleID: string,
    email: string,
    username: string,
    folder: FolderPropsInterface
}