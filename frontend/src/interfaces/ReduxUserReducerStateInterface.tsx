import { UserInterface } from "./UserInterface";

export interface ReduxUserReducerStateInterface {
    loading: boolean,
    error: string,
    user: any,
    isAuthenticated: boolean
}