import { FileInterface } from "./FileInterface";

export interface FolderPropsInterface {
    name: string,
    type: string,
    _id: string,
    files: FileInterface[],
    folders: FolderPropsInterface[],
    updatedAt: Date,
    uploadedBy?: string
}