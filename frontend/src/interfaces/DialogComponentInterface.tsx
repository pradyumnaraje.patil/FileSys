export interface DialogComponentInterface {
    open: boolean;
    title: string,
    onDialogClose: () => void,
    uploadFile: (fileData: any) => void
}