import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';
import React from 'react';

interface Message {
    open: boolean,
    message: string,
    onClose: () => void
}

function DialogMessage(props: Message) {
    return (
        <Dialog open={props.open} fullWidth={true} maxWidth="sm" onClose={ props.onClose }>
            <DialogTitle>Error</DialogTitle>
            <DialogContent>
                { props.message }
            </DialogContent>
            <br />
            <DialogActions>
                <Button color="primary" variant="contained" onClick={ props.onClose }>Close</Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogMessage;