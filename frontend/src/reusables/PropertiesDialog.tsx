import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { PropertiesDialogInterface } from '../interfaces/PropertiesDialogInterface';
import filesize from 'filesize';
import axios from 'axios';
import { SERVER_URL } from '../components/data/Variables';
import { getHeader } from '../utils/getHeader';
import { FileInterface } from '../interfaces/FileInterface';

// TYPE 0 FOR FOLDER & TYPE 1 FOR FILE & 2 FOR BOTH
interface tableDataInterface {
    heading: string,
    fieldName: string,
    type: number
}

const tableData = [
    {
        heading: "Name",
        fieldName: "name",
        type: 2
    },
    {
        heading: "Files",
        fieldName: "files",
        type: 0
    },
    {
        heading: "Folders",
        fieldName: "folders",
        type: 0
    },
    {
        heading: "Date Modified",
        fieldName: "updatedAt",
        type: 2
    },
    {
        heading: "Type",
        fieldName: "type",
        type: 2
    },
    {
        heading: "Size",
        fieldName: "size",
        type: 2
    }
]

interface IIndexable {
    [key: string]: any;
  }

function PropertiesDialog(props: PropertiesDialogInterface) {
    const [size, setSize] = useState(-1);
    useEffect(():any => {
        getSize();
    },[])

    async function getSize(){
        if(props.file.type == 'dir') {
            let result = await axios.get(`${SERVER_URL}/folder/${props.file._id}/properties`,getHeader());
            setSize(result.data.data);
            console.log(result.data.data)
        }
    }
    return (
        <Dialog open={props.open} onClose={props.onClose} maxWidth="xs" fullWidth={true} className="text-left">
            <DialogContent>
            <table className="table">
                <tbody>
                    { 
                        tableData.map( (t, idx ) => (
                              <tr key={idx}>
                                { t.type == 2 &&
                                        <>
                                        <th scope="row">{ t.heading }</th>
                                        { (t.heading == "Size") 
                                            ?
                                            (size == -1 && props.file.type != 'dir') ? 
                                            <td>{filesize((props.file as FileInterface).size || 0)}</td>
                                                :
                                            <td>{(typeof(size) == 'number' && filesize(size)) || 0}</td>
                                            : 
                                            <td>{(props.file as IIndexable)[t.fieldName] }</td>
                                        }
                                        </>
                                }
                                { t.type == 0 && props.file.type == 'dir' &&
                                        <>
                                            <th scope="row">{ t.heading }</th>
                                            { 
                                            (t.heading == 'Files' || t.heading == 'Folders') ?
                                            <td>{(props.file as IIndexable)[t.fieldName].length } { t.heading }</td>
                                                :
                                            <td>{(props.file as IIndexable)[t.fieldName] }</td>
                                            }
                                        </>
                                }
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            </DialogContent>
        </Dialog>
    )
}

export default PropertiesDialog;