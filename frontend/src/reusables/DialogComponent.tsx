import { Dialog, DialogTitle, DialogContent, DialogContentText, Button, List, ListItem, DialogActions } from '@material-ui/core';
import { Label } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { DialogComponentInterface } from '../interfaces/DialogComponentInterface';
import filesize from 'filesize';

function DialogComponent(props: DialogComponentInterface) {
    const [uploadedFile,setUploadedFile] = useState({
        fileName: '',
        file: {
            size: '',
            type: ''
        }
    });

    useEffect(() => {
        setUploadedFile({
            fileName: '',
            file: {
                size: '',
                type: ''
            }
        });
    },[props])

    const handleFileUpload = (event: React.FormEvent<HTMLInputElement>) => {
        if((event.target as any).files[0].size > 1048576) {
            alert('Input file size should be less than 1mb')
            setUploadedFile({
                fileName: '',
                file: {
                    size: '',
                    type: ''
                }
            });
        } else {
            setUploadedFile({
                fileName: (event.target as any).files[0].name,
                file: (event.target as any).files[0]
            });
        }
    }

    return (
        <Dialog open={props.open} onClose={props.onDialogClose} maxWidth="sm" fullWidth={true} >
            <DialogTitle className="bg-dark text-white">{props.title}</DialogTitle>
            <DialogContent  className="bg-dark  text-white text-center py-3">
                    <form>
                    <h6>1 MB is the limit</h6>
                    <p>Maximum limit of file storage is 10</p>
                    <Button type="button" color="default" variant="outlined" size="small" className="text-white border-white mb-2">
                        <label style={{cursor: 'pointer'}} htmlFor="file">
                            Choose file
                        </label>
                    </Button>
                    <input type="file" name="file" className="d-none" id="file" onInput={handleFileUpload} />
                    {uploadedFile.fileName != '' && 
                        <>
                            <List style={{backgroundColor: '#333'}}>
                                <ListItem>
                                    <>{uploadedFile.fileName}</>
                                </ListItem>
                            </List>
                            <div>
                                <small className="text-left font-weight-light w-100">Size: {filesize(parseInt(uploadedFile.file?.size))}</small>
                            </div>
                            <div>
                                <small className="text-left font-weight-light w-100">Type: {uploadedFile.file?.type}</small>
                            </div>
                        </>
                    }
                    </form>
            </DialogContent>
            <DialogActions className="bg-dark  text-white text-center py-3" >
                    <Button type="button" className="w-100 text-white border-white" variant="outlined" onClick={() => props.uploadFile(uploadedFile)}>
                        Upload
                    </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogComponent;