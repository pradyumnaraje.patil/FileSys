import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, TextField, InputAdornment, DialogActions, Button } from '@material-ui/core';
import { Mail } from '@material-ui/icons';
import axios from 'axios';
import { SERVER_URL } from '../components/data/Variables';
import { getHeader } from '../utils/getHeader';

interface DialogUserInterface {
    open: boolean,
    onClose: any,
    id: any,
    name: string
}

function DialogUsers(props: DialogUserInterface) {
    const [mail,setMail] = useState('');


    const handleChange = (event: any) => {
        setMail(event.target.value);
    }

    const close = () => {
        setMail('');
        props.onClose();
    }

    const shareFile = async () => {
        await axios.post(`${SERVER_URL}/file/${props.id}/share`,{ mails: mail }, getHeader());
        close();
    }


    const getMailFields = (
        <TextField value={mail} className="w-100" onInput={handleChange}  InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                    <Mail />
                    </InputAdornment>
                )
                }} 
            label="Email">

        </TextField>
    );
    return (
        <Dialog open={props.open} onClose={props.onClose} maxWidth="sm" fullWidth={ true }>
            <DialogTitle>Share <b>{ props.name }</b> </DialogTitle>
            <DialogContent>
                <p>Enter the email to share document <small>(multiple separated by comma)</small></p>
                { getMailFields }
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={close}>
                    Cancel
                </Button>
                <Button color="primary" variant="contained" onClick={shareFile} disabled={mail.trim() == ""}>
                    Share
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogUsers;