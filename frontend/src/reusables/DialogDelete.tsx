import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';

interface DeleteDialogProps {
    open: boolean,
    onClose: () => void,
    onSuccess: () => void,
    fileName: string
}
function DialogDelete(props: DeleteDialogProps) {
    return (
        <Dialog open={props.open} onClose={props.onClose} maxWidth="sm" fullWidth={true}>
            <DialogTitle>
                Delete Confirmation
            </DialogTitle>
            <DialogContent>
                Are you sure you want to delete { props.fileName } ?
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={ props.onSuccess }>
                    Yes
                </Button>
                <Button color="primary" variant="contained" onClick={ props.onClose }>
                    No
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogDelete;