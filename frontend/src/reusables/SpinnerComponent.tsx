import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { SpinnerInterface } from '../interfaces/SpinnerInterface';



const SpinnerComponent = (props: SpinnerInterface) => {
    return (
        <CircularProgress 
            className={props.className} 
            size={props.size} 
            disableShrink />
    )
}

export default SpinnerComponent;