import {
    FILE_URL_REQUEST,
    FILE_URL_SUCCESS,
    FILE_URL_ERROR,
    FILE_UPLOAD_REQUEST,
    FILE_UPLOAD_SUCCESS,
    FILE_UPLOAD_ERROR
} from './ActionTypes';
import { ActionInterface } from '../../interfaces/ActionInterface';
import { ReduxFileStateReducer } from '../../interfaces/ReduxFileStateReducer';

const initialState: ReduxFileStateReducer = {
    loading: false,
    message: '',
    error: ''
};

const fReducer = (state = initialState,action: ActionInterface) => {
    switch(action.type) {
        case FILE_URL_REQUEST: return {
                                    ...state,
                                    loading: true
                                }
        case FILE_URL_SUCCESS: return {
                                ...state,
                                message: action.payload
                              }
        case FILE_URL_ERROR: return {
                                ...state,
                                error: action.payload
                            }
        case FILE_UPLOAD_REQUEST: return {
                                    ...state,
                                    loading: true
                                }
        case FILE_UPLOAD_SUCCESS: return {
                                    ...state,
                                    message: action.payload
                                }
        case FILE_UPLOAD_ERROR: return {
                                    ...state,
                                    error: action.payload
                                }
        default: return state
    }
}

export default fReducer;