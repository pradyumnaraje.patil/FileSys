import {
    FILE_URL_REQUEST,
    FILE_URL_SUCCESS,
    FILE_URL_ERROR,
    FILE_UPLOAD_REQUEST,
    FILE_UPLOAD_SUCCESS,
    FILE_UPLOAD_ERROR
} from './ActionTypes';
import axios from 'axios';
import { SERVER_URL } from '../../components/data/Variables';
import { getHeader } from '../../utils/getHeader';
import Storage from '../../components/data/Firebase';
import { v4 as uuid4 } from 'uuid';

export const fileUrlRequest = () => {
    return {
        type: FILE_URL_REQUEST
    }
}

export const fileUrlSuccess = (message: string) => {
    return {
        type: FILE_URL_SUCCESS,
        payload: message
    }
}

export const fileUrlError = (error: string) => {
    return {
        type: FILE_URL_ERROR,
        payload: error
    }
}

export const fileUploadRequest = () => {
    return {
        type: FILE_UPLOAD_REQUEST
    }
}

export const fileUploadSuccess = (message: string) => {
    return {
        type: FILE_UPLOAD_SUCCESS,
        payload: message
    }
}

export const fileUploadError = (error: string) => {
    return {
        type: FILE_UPLOAD_ERROR,
        payload: error
    }
}

// Firebase Storage
export const fileUpload = (fileData: any,folderID: string | null) => {
    return async (dispatch: any) => {
        dispatch(fileUrlRequest);
        let id = uuid4();
        Storage.ref(id).put(fileData.file)
        .on('state_changed', () => {},() => {
            dispatch(fileUrlError('Error Occurred'));
        }, async () => {
            dispatch(fileUrlSuccess('Upload Progress'));

            let url = await Storage.ref(id).getDownloadURL();
            dispatch(fileUploadRequest);


            await axios.post(`${SERVER_URL}/file/${folderID}/new`, {
                folderID: folderID,
                name: fileData.file.name,
                type: fileData.file.type,
                link: url,
                size: fileData.file.size
            }, getHeader());
            dispatch(fileUploadSuccess('File has been uploaded successfully'));

            window.location.reload();
        });
    }
}


// AWS S3 Bucket
// export const fileUpload = (fileData: any,folderID: string | null) => {
//     return async (dispatch: any) => {
//         dispatch(fileUrlRequest);

//         let result = await axios.post(`${SERVER_URL}/file/url`,{ContentType: fileData.file.type},getHeader());
//         let url = result.data.url;
//         let key = result.data.key;


//         if( url == '' || key == '') {
//             dispatch(fileUrlError(result.data.message));
//         } else {
//             dispatch(fileUrlSuccess(result.data.message));

//             dispatch(fileUploadRequest);

//             try {
//                 // UPLOAD FILES TO s3 BUCKET

//                 await axios.put(url,fileData.file,{
//                                     headers: {
//                                     ContentType: fileData.file.type
//                                     }
//                                 });

//                 await axios.post(`${SERVER_URL}/file/${folderID}/new`, {
//                                                     folderID: folderID,
//                                                     name: fileData.file.name,
//                                                     type: fileData.file.type,
//                                                     link: key,
//                                                     size: fileData.file.size
//                                                 }, getHeader());
//                 dispatch(fileUploadSuccess('File has been uploaded successfully'));
                
//                 window.location.reload();
//             } catch(e) {
//                 dispatch(fileUploadError(e.message));
//             }
//         }
//     }
// }


