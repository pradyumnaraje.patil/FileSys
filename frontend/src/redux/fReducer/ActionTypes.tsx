export const FILE_URL_REQUEST: string = 'FILE_URL_REQUEST';
export const FILE_URL_SUCCESS: string = 'FILE_URL_SUCCESS';
export const FILE_URL_ERROR: string = 'FILE_URL_ERROR';

export const FILE_UPLOAD_REQUEST: string = 'FILE_UPLOAD_REQUEST';
export const FILE_UPLOAD_SUCCESS: string = 'FILE_UPLOAD_SUCCESS';
export const FILE_UPLOAD_ERROR: string = 'FILE_UPLOAD_ERROR';