import {
    USER_LOGIN_ERROR,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_REQUEST,
    USER_LOGOUT_SUCCESS
} from './ActionTypes';

import { UserInterface } from '../../interfaces/UserInterface';

// LOGIN REQUEST MADE
export const loginRequest = () => {
    return {
        type: USER_LOGIN_REQUEST
    }
}

// LOGIN REQUEST IS SUCCESSFUL
export const loginSuccess = (user: UserInterface) => {
    return {
        type: USER_LOGIN_SUCCESS,
        payload: user
    }
}

// LOGIN ERROR 
export const loginError = (error: string) => {
    return {
        type: USER_LOGIN_ERROR,
        payload: error
    }
}

// LOGOUT USER
export const logoutUser = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('exp');
    return {
        type: USER_LOGOUT_SUCCESS
    }
}
