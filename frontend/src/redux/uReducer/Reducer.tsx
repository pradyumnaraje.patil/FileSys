import {
    USER_LOGIN_ERROR,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_REQUEST,
    USER_LOGOUT_SUCCESS
} from './ActionTypes';
import { ActionInterface } from '../../interfaces/ActionInterface';
import { ReduxUserReducerStateInterface } from '../../interfaces/ReduxUserReducerStateInterface';
import { decodeJWT } from '../../utils/JwtDecode';

// INITITAL STATE OF USER REDUCER
let userToken = localStorage.getItem('user');
const initialState: ReduxUserReducerStateInterface = {
    loading: false,
    error: '',
    user: decodeJWT(userToken as any) || null,
    isAuthenticated: localStorage.getItem('user') !== null
}

const UserReducer = ((state: ReduxUserReducerStateInterface = initialState,action: ActionInterface) => {
    switch(action.type) {
        case USER_LOGIN_REQUEST: return {
                                    ...state,
                                    loading: true,
                                    isAuthenticated: false
                                }
        case USER_LOGIN_SUCCESS: return {
                                    ...state,
                                    user: action.payload,
                                    isAuthenticated: true
                                }
        case USER_LOGIN_ERROR: return {
                                    ...state,
                                    error: action.payload,
                                    isAuthenticated: false
                                }
        case USER_LOGOUT_SUCCESS: return {
                                    ...state,
                                    isAuthenticated: false,
                                    user: null
                                }
        default: return state;
    }
});

export default UserReducer;