export const USER_LOGIN_REQUEST: string = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS: string = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_ERROR: string = 'USER_LOGIN_ERROR';

export const USER_LOGOUT_SUCCESS: string = 'USER_LOGOUT_SUCCESS';