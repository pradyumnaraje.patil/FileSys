import { combineReducers,createStore, applyMiddleware } from 'redux';
import UserReducer from './uReducer/Reducer';
import FileReducer from './fReducer/Reducer';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { logger } from 'redux-logger';

// CREATE A REDUX STORE WITH REDUCERS & DEV TOOLS
const store = createStore(
    combineReducers({
        User: UserReducer,
        File: FileReducer
    }),
    composeWithDevTools(applyMiddleware(logger,thunk))
);

export default store;
