// NOT USED YET
import React, { useEffect } from 'react';

function useOutsideClick(ref: React.RefObject<any>, callback: any) {
    const handleClick = (event: MouseEvent) => {
        console.log('Hello');

        if(!ref.current || ref.current.contains(event.target)) {
            return;
        }
        console.log('Hello');
        callback(event);
    }

    useEffect(() => {
        document.addEventListener('click',handleClick);

        return () => {
            document.removeEventListener('click',handleClick);
        }
    },[ref,callback]);
}

export default useOutsideClick;