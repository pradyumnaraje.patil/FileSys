export const isImage = (ext: string): boolean => {
    const exp = new RegExp(/((jpeg)|(png)|(jpg)|(svg)|(gif)|(tiff))$/g);
    return exp.test(ext);
}