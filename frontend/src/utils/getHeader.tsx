export const getHeader = () => {
    let token = localStorage.getItem('user');
    return {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }
}