export const getQueryParam = (search: string, queryParam: string): string | null => {
    return new URLSearchParams(search).get(queryParam);
}