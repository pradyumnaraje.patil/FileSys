import jwt_decode from 'jwt-decode';
import { UserInterface } from '../interfaces/UserInterface';

export const decodeJWT = (token: string): UserInterface | null => {
    if(token == null)
        return null;
    return jwt_decode(token);
}