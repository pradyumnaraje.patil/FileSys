module.exports = {
    DB_URL:'mongodb://db:27017/fileage',
    PORT:5000,
    SERVER_URL:'http://localhost:5000',
    CLIENT_URL:'http://localhost:5000',
    JWT_SECRET:'SECRET'
}