module.exports = {
    DB_URL:'mongodb://db:27017/fileage',
    PORT:5000,
    SERVER_URL:'https://filesys.tech',
    CLIENT_URL:'https://filesys.tech',
    JWT_SECRET:'SECRET'
}