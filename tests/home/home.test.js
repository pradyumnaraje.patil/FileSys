const keys = require('../../env/keys');
const Page = require('../helpers/CustomPage');
const CustomPage = require('../helpers/CustomPage');

let page;
beforeEach(async () => {
    page = await Page.build();
    await page.goto(`${keys.CLIENT_URL}`,{ waitUntil: 'networkidle0' });
});

afterEach(async () => {
    await page.close();
});

describe("Home Page Without Login", () => {

    //NAVBAR ICON IF PRESENT OR NOT
    test("Navbar Icon is present",async () => {
        await page.waitForSelector('#navbar-icon')
        let src = await page.getSrc('#navbar-icon');        
        expect(src).toBeTruthy();
    });

    //NAVBAR LOGIN BUTTON IS AVAILABLE OR NOT
    test('Navbar Login button is present',async () => {
        await page.waitForSelector('#navbar-login')

        let loginBtn = await page.getElement('#navbar-login');
        expect(loginBtn).toBeTruthy();
    });

    //NAVBAR LOGOUT BUTTON IS NOT PRESENT
    test('Navbar logout button is not present',async () => {
        let logoutBtn = await page.getElement('#navbar-logout');
        expect(logoutBtn).toBeFalsy();
    });

    describe('Sidebar testing',() => {
        beforeEach(async()=>{

            let sidebarMenu = await page.getElement('#sidebar-toggle');
            expect(sidebarMenu).toBeTruthy();
            await sidebarMenu.click();
            await page.waitForSelector('#sidebar-list-item');
        });

        test('Sidebar Logo is visible',async () => {
            await page.waitForSelector('#sidebar-icon')

            let sidebarIcon = await page.getSrc('#sidebar-icon');
            expect(sidebarIcon).toBeTruthy();
        });

        test('Sidebar List items are 3',async() => {
            await page.waitForSelector('#sidebar-list-item')

            let sidebarListItems = await page.getElements('#sidebar-list-item');
            expect(sidebarListItems.length).toBe(2);
        });

        test('Sidebar List items text',async () => {
            await page.waitForSelector('#sidebar-list-item')

            let sidebarListItems = await page.getContents('#sidebar-list-item');

            ['Home','About Us'].forEach((el,idx) => {
                expect(sidebarListItems[idx]).toContain(el);
            })
        });
    });


    // HOME SCREEN TEST
    test('Home handle files title',async () => {
        await page.waitForSelector('#handleFilesTitle')

        let txt = await page.getContent('#handleFilesTitle');
        expect(txt).toBe("WANT TO HANDLE FILES AND FOLDERS?");
    });

    // GET STARTED
    test('Get Started button',async () => {
        
        await page.waitForSelector('#getStarted');
        let btn = await page.getContent('#getStarted');
        expect(btn).toBe('GET STARTED');
    });
});