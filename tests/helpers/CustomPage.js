const puppeteer = require('puppeteer');

class CustomPage {
    static async build() {
        const browser = await puppeteer.launch({
            args: [
                "--no-sandbox",
                "--window-size=1920,1080",
                "--disable-setuid-sandbox"
            ],
            defaultViewport: {width: 1920,height:1080 }
        });
        const page = await browser.newPage();

        const newPage = new CustomPage(page);
        const proxy = new Proxy(newPage,{
            get: (target,prop) => {
                return target[prop] || browser[prop] || page[prop];
            }
        });
        return proxy;
    }

    constructor(page) {
        this.page = page;
    }

    async getSrc(selector) {
        return await this.page.$eval(selector, el => el.src);
    }

    async getElement(selector) {
        return await this.page.$(selector)
    }
    async getElements(selector) {
        return await this.page.$$(selector);
    }

    async getContent(selector) {
        return await this.page.$eval(selector,el => el.innerText.trim());
    }
    
    async getContents(selector) {
        return await this.page.$$eval(selector,el => el.map(e => e.textContent.trim()));
    }
}

module.exports = CustomPage;
