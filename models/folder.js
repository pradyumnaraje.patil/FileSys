const mongoose = require('mongoose');

const folderSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        default: 'dir'
    },
    files: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Files'
    }],
    folders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Folders'
    }],
    uploadedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    }
},{
    timestamps: true
});

module.exports = mongoose.model('Folders',folderSchema);