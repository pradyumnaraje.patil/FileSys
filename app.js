const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
// ENVIRONMENT FILES
const keys = require('./env/keys');
require('dotenv').config();

const passport = require('passport');
// REQUIRE GOOGLE OAUTH STRATEGY
const JWTStrategy = require('./strategy/passport-jwt');
const GoogleStrategy = require('./strategy/google');


const cors = require('cors');
// DB
const db = require('./db/db');

// MODELS
require('./models/file');
require('./models/folder');

// ROUTES
const authRoutes = require('./routes/auth');
const folderRoutes = require('./routes/folder');
const fileRoutes = require('./routes/file');

const app = express();
const PORT = keys.PORT || 5000;

app.use(cors());
app.use(passport.initialize());
app.use(bodyParser.json());



app.use('/api/auth',authRoutes);
app.use('/api/folder',folderRoutes);
app.use('/api/file',fileRoutes);


if(process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'ci' || process.env.NODE_ENV === 'test') {
    app.use(express.static(path.join(__dirname,'frontend','build')));
    app.use('*',async(req,res) => {
        res.sendFile(path.join(__dirname,'frontend','build','index.html'));
    });
} else {
    app.get('/',async(req,res) => {
        res.send('Node API');
    });
}


app.listen(PORT,async (err) => {
    if(err) 
        console.log('Error: Server Unable to start');
    else 
        console.log(`Server listening on ${PORT}`);
});
