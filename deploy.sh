#!/bin/bash

set -o errexit

git stash

git pull origin production

cp ~/.env ~/FileSys/.env

docker-compose -f docker-compose.prod.yml up -d