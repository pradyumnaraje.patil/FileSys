const Folder = require('../models/folder');
const File = require('../models/file');
const mongoose = require('mongoose');
const isAuthorizedFolder = async (req, res, next) => {
    let id = req.params.id;
    let util = await Folder.aggregate([
        { $match: { _id: mongoose.Types.ObjectId(id) }},
        { $project: { uploadedBy: 1 }}
    ]);
   
    if(util[0] && util[0].uploadedBy && util[0].uploadedBy.equals(req.user._id)) 
        next();
    else
        return res.status(401).send('Unauthorized');
}

const isAuthorizedFile = async (req, res, next) => {
    let id = req.params.id;
    let util = await File.aggregate([
        { $match: { _id: mongoose.Types.ObjectId(id) }},
        { $project: { uploadedBy: 1 }}
    ]);
    if(util[0] && util[0].uploadedBy && util[0].uploadedBy.equals(req.user._id)) 
        next();
    else
        return res.status(401).send('Unauthorized');
}

module.exports = {
    isAuthorizedFolder,
    isAuthorizedFile
}