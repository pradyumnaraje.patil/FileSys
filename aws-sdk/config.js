const aws = require('aws-sdk');

let AWS =   new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    region: 'ap-south-1'
});

module.exports = AWS;