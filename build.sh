#!/bin/bash

set -o errexit

# Install backend dependancies
npm install

# Install frontend dependancies & Build frontend
CI=false npm run client-install
echo $PWD
CI=false npm run client-build


