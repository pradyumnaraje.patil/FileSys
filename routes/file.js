const express = require('express');
const AWS = require('../aws-sdk/config.js');
const { v4: uuid } = require('uuid');
const router = express.Router();
const passport = require('passport');
const mime = require('mime-types');
const File = require('../models/file');
const Folder = require('../models/folder');
const { deleteFile } = require('../utils/deleteFile.js');
const { isAuthorizedFolder, isAuthorizedFile } = require('../middlewares/isAuthorized');
const User = require('../models/user');
const mongoose = require('mongoose');
const { shareFile } = require('../utils/shareFile');

router.post('/url', passport.authenticate('jwt',{session: false}),
                async (req, res) =>{
    try {

        if(req.user.limit < 10) {
            let key = req.user._id + "/" + uuid() + "." + mime.extension(req.body.ContentType);
            let url = await AWS.getSignedUrl('putObject',{
                Bucket: process.env.BUCKET_NAME,
                Key: key,
                ContentType: req.body.ContentType
            });

            await User.findByIdAndUpdate(req.user._id,{ $inc: { limit: 1 }  });

            res.send({error: false, message: 'Signed Url received', url, key });
        } else {
            res.send({error: true, message: 'You have exceeded the limit to store files', url: '', key: ''});
        }
    } catch(e) {
        console.log(e);
        res.send({error: true, message: 'Signed Url not received',url: '',key: ''});
    }
});


router.post('/:id/new',passport.authenticate('jwt',{session: false}),
                    isAuthorizedFolder,
                    async (req, res) => {
        try {
            const { folderID, name, type, link, size } = req.body;
            const newFile = new File({
                name: name,
                type: type,
                link: `${link}`,
                size: size,
                uploadedBy: req.user._id
            });
            // SAVE THE NEW FILE DATA
            await newFile.save();

            // UPDATE THE FOLDER DATA TO INCLIDE THIS FILE
            await Folder.findOneAndUpdate({_id: folderID},{
                                                $push: { files: newFile._id}
                                            });

            res.send({error: false, message: 'File has been uploaded' })
        } catch(e) {
            console.log(e);
            res.send({error: true, message: 'File upload failed'});
        }                
});

router.put('/:id/rename',
        passport.authenticate('jwt',{session: false}),
        isAuthorizedFile,
        async (req, res) => {
            try {
                const { name } = req.body;
                await File.findByIdAndUpdate(req.params.id,{$set: { name }});
                res.send({error: false, message:'File has been renamed'});
            } catch(e) {
                console.log(e);
                res.send({error: true, message: 'Rename file failed'});
            }
});


router.delete('/:id',passport.authenticate('jwt',{session: false}),
    isAuthorizedFile,
    async (req, res) => {
        try {
            await deleteFile(req.user._id,req.params.id);
            res.send({error: false, message: 'File Deletion Success'});
        } catch(e) {
            console.log(e);
            res.send({error: true, message: 'File Deletion failed'});
        }
});

router.post('/:id/share', passport.authenticate('jwt',{ session: false }),
    isAuthorizedFile,
    async (req, res) => {
        try {
            let file = await File.aggregate([
                {$match: {_id: mongoose.Types.ObjectId(req.params.id)}},{$project: {link: 1}}
            ]);
            await shareFile(req.body.mails,file[0].link,req.user.username,req.user.email);
            res.send({error: false, message: 'File has been shared'});
        } catch(e) {
            console.log(e);
            res.send({error: true, message: 'File sharing failed'});
        }
});



module.exports = router;