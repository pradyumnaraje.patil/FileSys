const express = require('express');
const Folder = require('../models/folder');
const passport = require('passport');
const calculateFolderSize = require('../utils/calculateSize');
const router = express.Router();
const { deleteFolder } = require('../utils/deleteFile.js');
const { isAuthorizedFolder } = require('../middlewares/isAuthorized');


router.post('/:id/new', passport.authenticate('jwt',{session: false}),
                        isAuthorizedFolder,
                        async (req, res) => {
    try{
        const { name } = req.body;
        let newFolder = new Folder({ name,uploadedBy: req.user._id });
        // ADD THIS FOLDER TO CURRENTFOLDER
        await Folder.findByIdAndUpdate(req.params.id,
                                        {
                                            $push: {folders: newFolder._id}
                                        });
        await newFolder.save();
        res.status(200)
           .send({error: false, message: 'New Folder has been created'});
    } catch(e) {
        console.log(e);
        res.status(400)
           .send({error: true,message: 'Folder could not be created',data: []});
    }
});


router.get('/:id', passport.authenticate('jwt', { session: false }),
                isAuthorizedFolder,
                async (req, res) => {
    try {
        let folder = await Folder.findById(req.params.id)
                                         .populate('folders')
                                         .populate('files')
                                         .populate('uploadedBy');
        res.status(200)
           .send({error: false, message: 'Folder Information Retrieval Successful',data: folder})
    } catch(e) {
        console.log(e,'sdfsfd');
        res.status(404)
           .send({error: true,message: 'Folder information does not exists',data: {}});
    }
});

router.put('/:id/rename', passport.authenticate('jwt',{session: false}),
                        isAuthorizedFolder,
                        async (req, res) => {
    try{
        const { name } = req.body;
        // ADD THIS FOLDER TO CURRENTFOLDER
        let renameFolder = await Folder.findByIdAndUpdate(req.params.id,{
                                                $set: {
                                                    name
                                                }});
        res.status(200)
           .send({error: false, message: 'Folder name has been updated'});
    } catch(e) {
        console.log(e);
        res.status(400)
           .send({error: true,message: 'Folder name cannot be renamed',data: []});
    }
});

router.get('/:id/properties',passport.authenticate('jwt',{ session: false}),
    isAuthorizedFolder,
    async(req, res) => {
        try {
            let size = await calculateFolderSize(req.params.id);
            res.send({error: false, message: 'Folder properties retrieved',data: size});
        } catch(e) {
            console.log(e);
            res.send({error: true, message: 'Folder properties cannot be retrieved',data: 'Unknown'});
        }
});

router.delete('/:id',passport.authenticate('jwt',{session: false}),
    isAuthorizedFolder,
    async (req, res) => {
        try {
            await deleteFolder(req.user._id,req.params.id)

            res.send({error: true, message: 'Folder Deletion Success'});
        } catch(e) {
            console.log(e);
            res.send({error: true, message: 'Folder Deletion failed'});
        }
});





module.exports = router;