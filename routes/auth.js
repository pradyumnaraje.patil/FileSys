const router = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const keys = require('../env/keys');

// ROUTE WHICH WILL TAKE USER TO GOOGLE OAUTH SCREEN
router.get('/google', 
            passport.authenticate('google',{session: false,scope: ['profile','email']}),
            async (req, res) => {
                res.send('Authentication router');
            });

router.get('/google/callback', 
            passport.authenticate('google',{session: false}),
            async(req, res) => {
                console.log(req.user);
                let token = jwt.sign(JSON.stringify(req.user),
                                     keys.JWT_SECRET);
                res.redirect(`${keys.CLIENT_URL}/google/qfTT5TSkhFKupDtbohnwPheYFODWcmkr9HgZ8MbAElk09VzJPxKBPLRpZVtHQCfENK2lFUBSIc4XY7O35pigP4TK597p0XzYqugB?token=${token}`);
            });

module.exports = router;
