const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
// IMPORT USER MODEL
const User = require('../models/user');
const keys = require('../env/keys');
const Folder = require('../models/folder');

console.log(process.env.NODE_ENV);

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: `${keys.SERVER_URL}/api/auth/google/callback`
},async (accessToken,refreshToken,profile,done) => {
    // EXTRACT ID FROM profile parameter
    let id = profile.id;
    // FIND IF USER ALREADY EXISTS
    let user = await User.findOne({googleID: id}).populate('folder');
    // IF user EXISTS
    if(user)
        done(null,user);
    else {
        let newUser = new User();
        let newFolder = new Folder({
            name: 'Main Directory',
            uploadedBy: newUser._id
        });
        await newFolder.save();

        newUser.googleID = id;
        newUser.email = profile._json.email;
        newUser.username = profile._json.name;
        newUser.folder = newFolder._id;

        // SAVE THE newUser INTO DATABASE
        await newUser.save();
        newUser.folder = newFolder;
        done(null,newUser);
    }
}));