const passport = require('passport');
const keys = require('../env/keys');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const User = require('../models/user');

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: keys.JWT_SECRET,
},async (payload,done) => {
    const { googleID, _id } = payload;
    let user = await User.findOne({googleID,_id});

    if(user)
        return done(false,user);
    else
        return done(true,null)
}));