# FROM node:14.16.0-buster as frontend

# RUN mkdir -p /usr/app

# WORKDIR /usr/app

# COPY ./frontend/package*.json ./

# RUN npm install

# COPY ./frontend .

# RUN npm run build

# SERVER
FROM node:14.16.0-buster

WORKDIR /usr/src/app/

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 5000

CMD ["npm","start"]



