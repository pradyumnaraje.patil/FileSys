const mongoose = require('mongoose');
const Folder = require('../models/folder');

const calculateFolderSize = async (id) => {
    let size = 0;

    let data = await Folder.aggregate([
        { $match: { _id: mongoose.Types.ObjectId(id) } },
        {
            $lookup: {
                from: "files",
                localField: "files",
                foreignField: "_id",
                as: "file"
            }
        },
        {
            $project: {
                "folders": "$folders",
                "totalSize": { $sum: "$file.size" }
            }
        }
    ]);

    size += data[0].totalSize;

    for(let i = 0 ; i < data[0].folders.length ; i++ ) {
        size += await calculateFolderSize(data[0].folders[0]);
    }
    return size;
}

module.exports = calculateFolderSize;