const nodemailer = require('nodemailer');
const { google } = require('googleapis');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const OAuth2 = google.auth.OAuth2;

const createTransport = async () => {
    const oauth2Client = new OAuth2(
        process.env.MAIL_GOOGLE_CLIENT_ID, // ClientID
        process.env.MAIL_GOOGLE_CLIENT_SECRET, // Client Secret
        "https://developers.google.com/oauthplayground" // Redirect URL
   );
   oauth2Client.setCredentials({
    refresh_token: process.env.MAIL_REFRESH_TOKEN
});
const accessToken = oauth2Client.getAccessToken()

    const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            type: 'OAuth2',
            user: process.env.EMAIL,
            clientId: process.env.MAIL_GOOGLE_CLIENT_ID,
            clientSecret: process.env.MAIL_GOOGLE_CLIENT_SECRET,
            refreshToken: process.env.MAIL_REFRESH_TOKEN,
            accessToken: accessToken
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    return transporter;
}

const shareFile = async (mails,link,username,email) => {

    // NODEMAILER
    // const transporter = await createTransport();

    // console.log(mails,link,username);
    let allMails = mails.replace(/\s+/g,'').split(/,/g);
    // console.log(allMails);
    // for(let i = 0 ; i < allMails.length ; i++ ) {
    //     transporter.sendMail({
    //         subject: 'File shared by ' + username,
    //         html: `Download File: <a href=${link}>Click Here</a>`,
    //         to: allMails[i],
    //         from: process.env.EMAIL
    //     });
    // }

    // SENDGRID
    for(let i = 0 ; i < allMails.length ; i++ ) {
        let html = `Download File: <a href=${link}>Click Here</a>`;

        const msg = {
            to: allMails[i],
            from: process.env.EMAIL,
            subject: 'File shared by ' + username,
            html
        };
        try {
            let mail = sgMail.send(msg);
            console.log(mail);
        } catch (e) {
            console.error(e);
            if (e.response) {
                console.error(e.response.body);
            }
        }
    }

}


module.exports = {
    shareFile
}
