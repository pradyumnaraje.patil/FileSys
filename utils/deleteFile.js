const File = require('../models/file');
const Folder = require('../models/folder');
const AWS = require('../aws-sdk/config');
const mongoose = require('mongoose');

const deleteFile = async (user_id,id) => {
    try{
    let file = await File.findByIdAndRemove(mongoose.Types.ObjectId(id));
    // let splits = file.link.split('/');
    // let url = user_id + "/" + splits[splits.length - 1];

    // const params = {
    //     Bucket: process.env.BUCKET_NAME,
    //     Key: url
    // }
    // await AWS.deleteObject(params,(err,data) => {
    //     if(err) console.log(err);
    //     console.log(data);
    //     console.log('Deleted File');
    // });
    } catch(e) {
        console.log(e);
    }
}

const deleteFolder = async (user_id,id) => {
    let deletedFolder = await Folder.findByIdAndRemove(mongoose.Types.ObjectId(id));

    for(let i = 0 ; i < deletedFolder.folders.length ; i++) 
        await deleteFolder(user_id,deletedFolder.folders[i]);
    
    for(let i = 0 ; i < deletedFolder.files.length ; i++) 
        await deleteFile(user_id,deletedFolder.files[i]);
}

module.exports = {
    deleteFile,
    deleteFolder
}

